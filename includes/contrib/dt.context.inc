<?php

/**
 * @file Expanded API for context.module
 */

/**
 * @defgroup dt_context Context plugins definitions
 * @{
 */

/**
 * Implementation of hook_context_plugins().
 */
function dt_context_plugins() {
  $plugins = array();

  $plugins['dt_condition_spaces_type'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'dt') .'/plugins/context',
      'file' => 'dt_condition_spaces_type.inc',
      'class' => 'dt_condition_spaces_type',
      'parent' => 'context_condition',
    ),
  );
  $plugins['dt_condition_spaces_preset'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'dt') .'/plugins/context',
      'file' => 'dt_condition_spaces_preset.inc',
      'class' => 'dt_condition_spaces_preset',
      'parent' => 'context_condition',
    ),
  );

  return $plugins;
}

/**
 * Implementation of hook_context_registry().
 */
function dt_context_registry() {
  $registry = array();
  $registry['conditions'] = array(
    'dt_spaces_type' => array(
      'title' => t('Spaces Type'),
      'description' => t('Set this context when a viewer is within a space of the given type.'),
      'plugin' => 'dt_condition_spaces_type',
    ),
    'dt_spaces_preset' => array(
      'title' => t('Spaces Preset'),
      'description' => t('Set this context when a viewer is within a space of the given preset.'),
      'plugin' => 'dt_condition_spaces_preset',
    ),
  );
  return $registry;
}

/**
 * Implementation of hook_context_page_condition().
 */
function dt_context_page_condition() {
  if ($plugin = context_get_plugin('condition', 'dt_spaces_type')) {
    $plugin->execute(spaces_get_space());
  }
  if ($plugin = context_get_plugin('condition', 'dt_spaces_preset')) {
    $space = spaces_get_space();
    if (is_object($space)) {
      $space_preset = variable_get('spaces_preset_'. $space->type, NULL);
      $plugin->execute($space_preset);
    }
  }
}

/**
 * @} End of "defgroup dt_context".
 */


/**
 * @defgroup dt_hooks_context Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_nodeapi().
 */
function dt_context_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'view':
      if ($page && module_exists('context')) {
        context_set('dt_content', 'node_type_active_'. $node->type);
      }
      break;
  }
}

/**
 * @} End of "defgroup dt_hooks_context".
 */
