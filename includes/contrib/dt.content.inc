<?php

/**
 * @file Expanded API for content.module
 */

/**
 * Delete a set of fields in a set of content type (bulk)
 *
 * @param Array $mappings
 *   An array keyed by content_type associated with an array of fields to delete
 */
function dt_content_delete_fields($mappings) {
  foreach ($mappings as $type => $fields) {
    foreach ($fields as $field) {
      dt_content_field_delete($field, $type, TRUE);
    }
  }
}

/**
 * Delete a field
 *
 * @param String $field
 *   Name of the field to delete
 * @param String $type
 *   The content type to delete it from. Use _global to delete from all types.
 * @param String $cache
 *   Clear the content type cache after deleting.
 *
 */
function dt_content_field_delete($field, $type, $cache = TRUE) {
  module_load_include('inc', 'content', 'includes/content.crud');
  if ($type == '_global') {
    $types = array_keys(node_get_types('names'));
  }
  else {
    $types = array($type);
  }
  // Activate fields before deletion. Otherwise, they won't be deleted properly.
  db_query('UPDATE {content_node_field} SET active = 1 WHERE field_name = "%s"', $field);
  $deleted = 0;
  foreach ($types as $type) {
    db_query('UPDATE {content_node_field_instance} SET widget_active = 1 WHERE field_name = "%s" AND type_name = "%s"', $field, $type);
    if (content_field_instance_read(array('field_name' => $field, 'type_name' => $type))) {
      $deleted += (content_field_instance_delete($field, $type, FALSE) ? 1 : 0);
    }
  }
  if (!empty($deleted) && !empty($cache)) {
    content_clear_type_cache(TRUE);
    menu_rebuild();
  }
}

/**
 * Find what table values of a field is stored in
 *
 * @param String $field
 *   Name of the field.
 * @return
 *   The name of the table its values are stored in.
 */
function dt_content_field_table($field_name) {
  // http://drewish.com/content/2010/06/correctly_accessing_cck_fields_in_sql_queries
  $field_info = content_database_info(content_fields($field_name));
  return $field_info['table'];
}

/**
 * Go through every possible buildmode and set the format to be used
 * for display settings
 *
 * @param $settings
 *   A base settings to set for each build mode
 *
 * @return
 *   Array array of settings per build mode .
 */
function dt_content_field_display_settings($settings = NULL) {
  if (!isset($settings)) {
    $settings = array('format' => 'hidden', 'exclude' => 1);
    $display_settings['label'] = array('format' => 'inline', 'exclude' => 1);
  }
  else {
    $display_settings['label'] = array('format' => 'inline', 'exclude' => 0);
  }
  foreach (module_invoke_all('content_build_modes') as $mode) {
    foreach ($mode['build modes'] as $key => $info) {
      $display_settings[$key] = $settings;
    }
  }
  return $display_settings;
}

/**
 * Creates a string suitable for allowed values from key => value pears
 *
 * @param mixed $array
 *   An array of key => value
 */
function dt_content_field_create_allowed_values($array) {
  if (is_array($array)) {
    $vals = array();
    foreach ($array as $key => $value) {
      $vals[] = $key .'|'. $value;
    }
    return implode("\n", $vals);
  }
  return '';
}

/**
 * Changes allowed values in a form
 *
 * @param array $form
 *   Form by reference from hook_form_alter for a node type form
 * @param string $field
 *   Field to change
 * @param mixed $values
 *  Depending on the field type, Either a an array suitable for c
 *  ck_api_create_allowed_values or a string
 */
function dt_content_field_change_allowed_values(&$form, $field, $values) {
  if (empty($form[$field]['#after_build'])) {
    $form[$field]['#after_build'] = array();
  }
  $form[$field]['#after_build'][] = 'dt_content_field_limit_field';
  $form[$field]['#dt_content_field_new_values'] = $values;
}

function dt_content_field_limit_field($element) {
  if (isset($element['value'])) {
    $trim = &$element['value'];
  }
  elseif (isset($element['nid']['nid'])) {
    $trim = &$element['nid']['nid'];
  }
  if (!empty($trim) && !empty($trim['#options'])) {
    foreach ($trim['#options'] as $key => $title) {
      if (!in_array($key, array_keys($element['#dt_content_field_new_values']))) {
        unset($trim['#options'][$key]);
        unset($trim[$key]);
      }
      else {
        $trim[$key]['#title'] = $element['#dt_content_field_new_values'][$key];
      }
    }
  }
  return $element;
}

/**
 * Create any new fields
 */
function dt_content_field_install() {
 if ($fields = dt_features_get_default('content')) {
    module_load_include('inc', 'content', 'includes/content.crud');
    content_clear_type_cache(TRUE);

    foreach ($fields as $field) {
      // We use content_field_instance_read() here so that we can get inactive
      // fields too. We can't just pass $field to it as the fnc will add every
      // item of the array to the WHERE clause.
      $param = array('field_name' => $field['field_name'], 'type_name' => $field['type_name']);
      $existing_instance = array_shift(content_field_instance_read($param, TRUE));

      if (empty($existing_instance)) {
        // Summary: content_field_instance_create() will fall back to prior
        // instance values for weight, label, desc if not set.
        // See http://drupal.org/node/686240#comment-2786162
        $field['weight'] = $field['widget']['weight'];
        $field['label'] = $field['widget']['label'];
        $field['description'] = $field['widget']['description'];
        content_field_instance_create($field, FALSE);
      }
      variable_set('menu_rebuild_needed', TRUE);
    }
  }
}


/**
 * @defgroup dt_hooks_content Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_content_default_fields().
 */
function dt_content_content_default_fields() {
  $all_fields = module_invoke_all('content_default_fields_extra');
  // clean up
  $fields = array();
  // update to new keying system
  foreach ($all_fields as $field) {
    $fields[content_features_identifier($field)] = $field;
  }
  return $fields;
}

/**
 * Implementation of hook_flush_caches().
 */
function dt_content_flush_caches() {
  if (variable_get('dt_install', 0)) {
    dt_content_field_install();
  }
}

/**
 * @} End of "defgroup dt_hooks_content".
 */
