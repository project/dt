<?php

/**
 * @file Expanded API for flag.module
 */

/**
 * Flags or unflags an item.
 *
 * @param $account
 *   The user on whose behalf to flag. Leave empty for the current user.
 * @return
 *   FALSE if some error occured (e.g., user has no permission, flag isn't
 *   applicable to the item, etc.), TRUE otherwise.
 */
function dt_flag($action, $flag_name, $content_id, $account = NULL) {
  if (!($flag = flag_get_flag($flag_name))) {
    // Flag does not exist.
    return FALSE;
  }
  return $flag->flag($action, $content_id, $account, TRUE);
}
