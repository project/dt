<?php

/**
 * @file Expanded API for fieldgroup.module
 */

/**
 * Add a fieldgroup
 *
 * @param String $group_name
 *   The human readable name of the group.  This will automatically be
 *   converted into the group's machine name as well.
 * @param String $style
 *   The style of group, such as 'tabs'.
 * @param Array $content_types
 *   An array of content types' machine names to have this group
 * @param Integer $weight
 *   Weight of the form in relation to the other groups and fields on the form
 */
function dt_fieldgroup_add($group_name, $style, $content_types, $weight = 0) {
  $default = array(
    'group_type' => 'standard',
    'settings' => array(
      'form' => array(
        'style' => $style,
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        5 => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        4 => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        2 => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        3 => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'label' => 'above',
      ),
    ),
    'weight' => $weight,
  );

  foreach ($content_types as $content_type) {
    $group_content = $default;
    $group_content['label'] = $group_name;
    $group_content['group_name'] = 'group_'. str_replace(' ', '_', strtolower($group_name));
    fieldgroup_save_group($content_type, $group_content);
  }
}


/**
 * Delete a fieldgroup from a content type.
 *
 * @param String $group
 *   Name of the $group to delete
 * @param String $type
 *   The content type to delete it from. Use _global to delete from all types.
 */
function dt_fieldgroup_delete($group, $type) {
  if ($type == '_global') {
    $types = array_keys(node_get_types('names'));
  }
  else {
    $types = array($type);
  }
  foreach ($types as $type) {
    // Not harmful to run without any checks, so run it.
    fieldgroup_delete($type, $group);
  }
}

/**
 * Reverts all fieldgroups.
 */
function dt_fieldgroup_install() {
  $revert = array();
  features_include_defaults('fieldgroup');
  $modules = module_implements('fieldgroup_default_groups');
  foreach ($modules as $module) {
    $revert[$module][] = 'fieldgroup';
  }
  if (!empty($revert)) {
    features_revert($revert);
  }
}


/**
 * @defgroup dt_hooks_fieldgroup Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_flush_caches().
 */
function dt_fieldgroup_flush_caches() {
  if (variable_get('dt_install', 0)) {
    dt_fieldgroup_install();
  }
}

/**
 * @} End of "defgroup dt_hooks_fieldgroup".
 */
