<?php

/**
 * @file Expanded API for og.module
 */

/**
 * Retrieve all user subscriptions to groups of a specified node type.
 *
 * @param $uid
 *  User account id.
 * @param $type
 *  Group node type.
 * @param $min_is_active
 *  (default: 1) Subscription status.
 *  - 1: Active subscriptions.
 *  - 0: Active and pending subscriptions.
 *
 * @return Array
 * @see og_get_subscriptions
 *
 * @note This can be pushed up into og.module probably.
 */
function dt_og_get_subscriptions_by_type($uid, $type, $min_is_active = 1, $reset = FALSE) {
  static $subscriptions;

  if (!isset($subscriptions[$uid][$min_is_active]) || $reset) {
    $subs = og_get_subscriptions($uid, $min_is_active, $reset);
    foreach ($subs as $gid => $sub) {
      if ($sub['type'] != $type) {
        unset($subs[$gid]);
      }
    }
    $subscriptions[$uid][$min_is_active][$type] = $subs;
  }

  return $subscriptions[$uid][$min_is_active][$type];
}

function dt_og_all_groups_options($types, $current = FALSE, $exclude = FALSE) {
  global $user;
  if (empty($types)) {
     $types = og_get_types('group');
  }

  $args = $types;
  $sql = "SELECT n.title, n.nid FROM {node} n WHERE n.type IN (" .  db_placeholders($types, 'varchar') . ") AND n.status = 1";

  if (!empty($current)) {
    if (empty($user->og_groups)) {
      return array();
    }
    $groups = array_keys($user->og_groups);
    $sql .= " and n.nid in (" . db_placeholders($groups) . ")";
    $args = array_merge($args, $groups);
  }

  if (!empty($exclude)) {
    $sql .= " and n.uid <> %d";
    $args[] = $user->uid;
  }

  $sql .= " ORDER BY n.title ASC";
  $result = db_query($sql, $args);
  while ($row = db_fetch_object($result)) {
    $options[$row->nid] = check_plain($row->title);
  }

  return $options ? $options : array();
}
