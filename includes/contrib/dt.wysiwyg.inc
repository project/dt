<?php

/**
 * @file Expanded API for wysiwyg.module
 */

/**
 * Configure WYSIWYG settings for an input format.  Create entry if none exists.
 *
 * @param Integer $format_id
 * @param String $editor
 * @param Array $settings
 * @return Array
 *   The updated profile settings for the given format
 */
function dt_wysiwyg_configure_format($format_id, $editor, $settings) {
  $exists = wysiwyg_get_profile($format_id);
  if ($exists) {
    db_query("UPDATE {wysiwyg} SET editor = '%s', settings = '%s' WHERE format = %d",
      $editor, serialize($settings), $format_id);
  }
  else {
    db_query("INSERT INTO {wysiwyg} (format, editor, settings) " .
        "VALUES (%d, '%s', '%s')", $format_id, $editor, serialize($settings));
  }

  $result = wysiwyg_get_profile($format_id);
  return $result;
}

/**
 * Wrapper for dt_wysiwyg_configure_format
 */
function dt_wysiwyg_save($format) {
  dt_wysiwyg_configure_format($format->format, $format->editor, $format->settings);
  $result = wysiwyg_get_profile($format_id);
  return $result;
}

/**
 * Wrapper for wysiwyg_get_profile
 */
function dt_wysiwyg_load($format_id) {
  return wysiwyg_get_profile($format_id);
}
