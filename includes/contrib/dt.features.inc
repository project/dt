<?php

/**
 * @file Expanded API for features.module
 */

/**
 * Wrapper for features revert.
 *
 * Makes sure feature exists before reverting.
 */
function dt_features_revert($revert = array()) {
  module_load_include('inc', 'features', 'features.export');
  $states = features_get_component_states(array(), FALSE, TRUE);
  if (empty($revert)) {
    foreach ($states as $feature_name => $state) {
      $revert[$feature_name] = array();
    }
  }
  elseif (is_string($revert)) {
    $revert = array($revert => array());
  }

  $restore_states = array(FEATURES_OVERRIDDEN, FEATURES_REBUILDABLE, FEATURES_NEEDS_REVIEW);

  // look for empty components and revert any that want to be reverted
  foreach ($revert as $feature_name => $feature_components) {
    if (!module_exists($feature_name)) {
      unset($revert[$feature_name]);
    }
    elseif (empty($feature_components) && !empty($states[$feature_name])) {
      $components = $states[$feature_name];
      foreach ($components as $component => $state) {
        if (in_array($state, $restore_states)) {
          $revert[$feature_name][] = $component;
        }
      }
    }
  }

  if (!empty($revert)) {
    features_revert($revert);
  }
}



/**
 * Remove data from features pipe
 *
 * @param $pipe
 *  Associated array of component => data to
 * @param $map
 *  Mapping of component => info to remove per node
 * @param $types
 *  Optional, array of content types to check. Defaults to all content types
 *
 * @return
 *   Array array of settings per build mode .
 */
function dt_features_prune_node_data(&$pipe, $map, $types = array()) {
  if (empty($types)) {
    $types = array_keys(node_get_types('names'));
  }
  // cannot use $pipe['node'] as it might not exist
  foreach ($types as $type) {
    foreach ($map as $component => $removes) {
      if (!empty($pipe[$component])) {
        foreach ($removes as $remove) {
          $key = array_search($type .'-'. $remove, $pipe[$component]);
          if ($key !== FALSE) {
            unset($pipe[$component][$key]);
          }
        }
      }
    }
  }
}

/**
 * Wrapper for features_get_default.
 */
function dt_features_get_default($component, $module_name = NULL, $alter = TRUE, $reset = FALSE) {
  module_load_include('inc', 'features', 'features.export');
  return features_get_default($component, $module_name, $alter, $reset);
}

/**
 * Return
 */
function dt_features_feature_of_component($type, $component = 'node') {
  $feature_map = features_get_component_map($component);
  // This is how features does it, so do it like this for now.
  if (!empty($feature_map[$type])) {
    return reset($feature_map[$type]);
  }
  return;
}
