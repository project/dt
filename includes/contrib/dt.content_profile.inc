<?php

/**
 * @file Expanded API for content.module
 */

/**
 * Update/creates a a content profile for a user
 *
 * Param $content_type
 *  Content type of content profile
 * Param $uid
 *  User UID to update profile for.
 * Param $values
 *  Values to add/change for user profile.
 *
 */
function dt_content_profile_update($content_type, $uid, $values = array()) {
  static $processing = array();
  $account = dt_user_load($uid);

  if (!empty($processing[$account->uid])) {
    return;
  }

  $processing[$account->uid] = TRUE;

  if ($profile = content_profile_load($content_type, $uid)) {
    $node = $profile;
    $insert = FALSE;
  }
  else {
    $insert = TRUE;
    $node = (object) array(
      'type' => 'profile',
      'uid' => $account->uid,
      'name' => $account->name,
      'dt_content_profile_auto_generated' => TRUE,
    );
  }

  foreach ($values as $key => $value) {
    $node->$key = $value;
  }

  node_save($node);

  // Reload content_profile_load's internal cache.
  if ($insert) {
    $language = isset($node->language) ? $node->language : '';
    content_profile_load($node->type, $node->uid, $language, TRUE);
  }

  $processing[$account->uid] = FALSE;
  return $node;
}
