<?php

/**
 * @file Expanded API for views.module
 */


/**
 * @defgroup dt_views Views plugins definitions
 * @{
 */

/**
 * Implementation of hook_views_api().
 */
function dt_views_api() {
  return array(
    'api' => '2',
  );
}

/**
 * Implementation of hook_views_data().
 */
function dt_views_data() {
  $data = array();
  $data['views']['has_exposed'] = array(
    'title' => t('Has Exposed Input'),
    'help' => t('Validates that view has any combination of exposed filters'),
    'argument' => array(
      'handler' => 'views_handler_argument_has_exposed',
    ),
  );
  $data['og']['not_self'] = array(
    'title' => t('Group not self'),
    'filter' => array(
      'handler' => 'dt_handler_filter_og_not_self',
      'numeric' => TRUE,
      'allow empty' => TRUE,
      'real field' => 'nid',
      'help' => t('Filter out the current group from the list.'),
    )
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function dt_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'dt') . '/plugins/views',
    ),
    'handlers' => array(
      // argument
      'dt_handler_argument_has_exposed' => array(
        'parent' => 'views_handler_argument_null',
      ),
      // filter
      'dt_handler_filter_og_not_self' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
 * @} End of "defgroup dt_views".
 */
