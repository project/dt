<?php

/**
 * @file Expanded API for noderelationships.module
 */

/**
 * Sets up the a content-type node-reference field
 *
 * Adapted from noderelationships_admin_settings_noderef_submit()
 *
 * @param String $content_type
 *   Machine-name of the content type
 * @param String $field
 *   Machine-name of the reference field to have the noderelationships system
 *   added to
 * @param String $view_type
 *   Type of view that will be presenting for searching for references.
 *   Currently supports 'table' and 'grid'.
 * @param Boolean $create_and_reference
 *   Whether or not a new node can be created instead of referencing an
 *   existing one.
 */
function dt_noderelationships_setup($content_type, $field, $view_type = 'table', $create_and_reference = TRUE, $view_in_new_window = TRUE) {
  $search_and_reference_view = 'noderelationships_noderef:page_'. $view_type;

  module_load_include('inc', 'noderelationships');
  module_load_include('inc', 'noderelationships.admin');
  $content_type = noderelationships_get_localized_content_type($content_type);

// TODO:  drupal_execute should work!  It's not working, so I'm copy/pasting the _submit function here which could be dangerous of that function changes.
//  $form_state['values']['op'] = 'Save settings';
//  drupal_execute('noderelationships_admin_settings_noderef', $form_state, $content_type);

  $reference_fields = noderelationships_get_reference_fields($content_type->type);

  // Obtain extra nodereference settings for this type.
  $noderef_settings = noderelationships_settings_load($content_type->type, 'noderef');

  $form_state['values']['create_and_reference'] = $noderef_settings['create_and_reference'];
  $form_state['values']['create_and_reference'][$field] = ( $create_and_reference ? $field : 0 );

  $form_state['values']['view_in_new_window'] = $noderef_settings['view_in_new_window'];
  $form_state['values']['view_in_new_window'][$field] = ( $view_in_new_window ? $field : 0 );

// TODO: add translate_and_reference stuff here, I'm not grocking noderelationship's form-submit on this part, wouldn't have to if drupal_execute worked.
//  if (module_exists('translation')) {
//    $form_state['values']['translate_and_reference'] = $noderef_settings['translate_and_reference'];
//  }

  $form['#noderelationships-type'] = $content_type->type;

  $form_state['values']['search_and_reference_view'] = $noderef_settings['search_and_reference_view'];
  $form_state['values']['search_and_reference_view'][$field] = $search_and_reference_view;


  $form_values = $form_state['values'];
  $settings = noderelationships_settings_load($form['#noderelationships-type']);

  // Move form values to settings array.
  $settings['noderef'] = array(
    'search_and_reference_view' => array_filter($form_values['search_and_reference_view']),
    'create_and_reference' => array_filter($form_values['create_and_reference']),
    'view_in_new_window' => array_filter($form_values['view_in_new_window']),
    'translate_and_reference' => (isset($form_values['translate_and_reference']) ? array_filter($form_values['translate_and_reference']) : array()),
  );

  // Update settings and clear caches if changes exist.
  if (noderelationships_settings_save($form['#noderelationships-type'], $settings)) {
    noderelationships_cache_clear_all();
  }
}

/**
 * Sets up a content-type so that it backreferences nodes that reference it
 * (from above function)
 *
 * Adapted from noderelationships_admin_settings_backref_submit()
 *
 * @todo figure out how this function works
 *
 * @param String $content_type
 *   Machine-name of the content type
 * @param String $referrer_type
 *   Machine-name of the content type that is refering to the $content_type
 * @param String $this_field
 * @param Integer $weight
 * @param String $back_reference_region
 * @param String $back_reference_view
 */
function dt_noderelationships_setup_backref($content_type, $referrer_type, $this_field, $weight = '0', $back_reference_region = 'field', $back_reference_view = '') {
  module_load_include('inc', 'noderelationships');
  module_load_include('inc', 'noderelationships.admin');

  $content_type = noderelationships_get_localized_content_type($content_type);
  $referrer_types = noderelationships_get_referrer_types($content_type->type);
  // Obtain back reference settings for this type.
  $backref_settings = noderelationships_settings_load($content_type->type, 'backref');

  // Prepare a list of all available relations.
  $available_relations = array();
  foreach ($referrer_types as $referrer_type => $field_names) {
    foreach ($field_names as $field_name) {
      $available_relations[$referrer_type .':'. $field_name] = array(
        'referrer_type' => $referrer_type,
        'field_name' => $field_name,
      );
    }
  }

  // Build the list of all relations assigned to active regions.
  $form['#noderelationships-regions'] = array();
  foreach (array_keys(noderelationships_get_back_reference_regions()) as $region) {
    $form['#noderelationships-regions'][$region] = array();
    if (isset($backref_settings['regions'][$region])) {
      foreach ($backref_settings['regions'][$region] as $relation_key => $relation_info) {
        if (isset($available_relations[$relation_key])) {
          $referrer_type = $available_relations[$relation_key]['referrer_type'];
          $field_name = $available_relations[$relation_key]['field_name'];
          $form['#noderelationships-regions'][$region][$relation_key] = array(
            'referrer_type' => $referrer_type,
            'field_name' => $field_name,
            'field' => content_fields($field_name, $referrer_type),
            'weight' => $relation_info['weight'],
            'back_reference_view' => $relation_info['back_reference_view'],
          );
          unset($available_relations[$relation_key]);
        }
      }
    }
  }

  // Assign remaining relations to the disabled region.
  $form['#noderelationships-regions']['none'] = array();
  foreach ($available_relations as $relation_key => $available_relation) {
    $referrer_type = $available_relation['referrer_type'];
    $field_name = $available_relation['field_name'];
    $form['#noderelationships-regions']['none'][$relation_key] = array(
      'referrer_type' => $referrer_type,
      'field_name' => $field_name,
      'field' => content_fields($field_name, $referrer_type),
      'weight' => 0,
      'back_reference_view' => '',
    );
  }
  unset($available_relations);


  $form['#noderelationships-type'] = $content_type->type;

  foreach ($form['#noderelationships-regions'] as $region => $region_relations) {
    foreach ($region_relations as $relation_key => $relation_info) {
      $referrer_type = $relation_info['referrer_type'];
      $field_name = $relation_info['field_name'];
      $field = $relation_info['field'];

      $form_state['values']['weight'][$relation_key] = (isset($relation_info['weight']) ? $relation_info['weight'] : 0);
      $form_state['values']['back_reference_region'][$relation_key] = $region;
      $form_state['values']['back_reference_view'][$relation_key] = (isset($relation_info['back_reference_view']) ? $relation_info['back_reference_view'] : '');
    }
  }

  $this_relation_key = $referrer_type .':'. $this_field;
  $form_state['values']['weight'][$this_relation_key] = $weight;
  $form_state['values']['back_reference_region'][$this_relation_key] = $back_reference_region;
  $form_state['values']['back_reference_view'][$this_relation_key] = $back_reference_view;

// TODO:  drupal_execute should work!  It's not working, so I'm copy/pasting the _submit function here which could be dangerous of that function changes.
//  $form_state['values']['op'] = 'Save settings';
//  drupal_execute('noderelationships_admin_settings_bacref', $form_state, $content_type);

  $form_values = $form_state['values'];
  $settings = noderelationships_settings_load($form['#noderelationships-type']);

  // Move form values to settings array.
  $settings['backref'] = array('regions' => array());
  foreach (array_keys(noderelationships_get_back_reference_regions()) as $region) {
    foreach ($form_values['back_reference_region'] as $relation_key => $relation_region) {
      if ($region == $relation_region) {
        if (!isset($settings['backref']['regions'][$region])) {
          $settings['backref']['regions'][$region] = array();
        }
        $settings['backref']['regions'][$region][$relation_key] = array(
          'weight' => $form_values['weight'][$relation_key],
          'back_reference_view' => $form_values['back_reference_view'][$relation_key],
        );
      }
    }
  }

  // Update settings and clear caches if changes exist.
  if (noderelationships_settings_save($form['#noderelationships-type'], $settings)) {
    noderelationships_cache_clear_all();
  }
}

/**
 * Allows the processing of an array to configure multiple fields
 * simultaneously
 *
 * @param String $content_type
 *   Machine-name of the content type whose fields will be configured
 * @param Array $fields
 *   An array of fields to configure.  Setup your array like one of these:
 *      array(
 *        'field_name_A' => array(),
 *        'field_name_B' => array(),
 *      );
 *      array(
 *        'field_name_A' => array(
 *          'view_type' => 'grid',
 *          'create' => TRUE,
 *        ),
 *        'field_name_B' => array(
 *          'create' => FALSE,
 *        ),
 *      );
 */
function dt_noderelationships_mass_setup($content_type, $fields) {
  foreach ($fields as $field => $conf) {
    $view_type = $conf['view_type'] ? $conf['view_type'] : 'table';
    $create_and_reference = array_key_exists('create', $conf) ? $conf['create'] : TRUE;
    $view_in_new_window = array_key_exists('view', $conf) ? $conf['view'] : TRUE;
    dt_noderelationships_setup($content_type, $field, $view_type, $create_and_reference, $view_in_new_window);
  }
}

/**
 * Returns an array of content types that have reciprocal references and their
 * reciprocal fields.
 *
 * @param String $content_type
 *   Machine-name of the content type whose fields should be returned
 * @return Array
 *   All recirprocal relationships and their fields, or just the relationships
 *   associated with the given content type.
 */
function dt_noderelationships_reciprocal($content_type = NULL) {
  $return_type = $content_type;

  module_load_include('inc', 'noderelationships', 'noderelationships');
  $relationships = noderelationships_get_relationships();

  $reciprocal = array();
  $content_types = array();
  foreach ($relationships['referrer'] as $content_type => $data) {
    $content_types[$content_type] = array();
    foreach ($data['referred_types'] as $referred_type => $fields) {
      if (array_key_exists($referred_type, $relationships['referrer'])) {
        $referred_referred_types = $relationships['referrer'][$referred_type]['referred_types'];
        if (array_key_exists($content_type, $referred_referred_types)) {
          // State this relationship in a structured fashion, alphabetical order
          $relationship_name = dt_noderelationships_reciprocal_name($referred_type, $content_type);

          // Store the information about this relationship
          $reciprocal[$relationship_name][$content_type] = $fields;
          $referred_fields = $relationships['referrer'][$referred_type]['referred_types'][$content_type];
          $reciprocal[$relationship_name][$referred_type] = $referred_fields;

          // Also save this information in content type specific array
          $content_types[$content_type][$relationship_name] = $reciprocal[$relationship_name];
        }
      }
    }
  }

  if ($return_type) {
    if (isset($content_types[$return_type])) {
      return $content_types[$return_type];
    }
    else {
      return array();
    }
  }
  else {
    return $reciprocal;
  }
}

/**
 * Generates a reciprocal relationship name
 */
function dt_noderelationships_reciprocal_name($content_type_a, $content_type_b) {
  if (strcmp($content_type_a, $content_type_b) < 0) {
    $relationship_name = $content_type_a .'-'. $content_type_b;
  }
  else {
    $relationship_name = $content_type_b .'-'. $content_type_a;
  }
  return $relationship_name;
}

/**
 * Gets all node reference fields and the content type(s) it refers to
 */
function dt_noderelationships_get_fields() {
  module_load_include('inc', 'noderelationships', 'noderelationships');
  $relationships = noderelationships_get_relationships();
  $reference_fields = $relationships['nodereferences'];

  $fields = array();
  foreach ($reference_fields as $field => $data) {
    foreach ($data as $referrer => $referred) {
      $content_type = $referred[0];
    }
    $fields[$field] = $content_type;
  }

  return $fields;
}
