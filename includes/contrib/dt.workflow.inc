<?php

/**
 * @file Expanded API for workflow.module
 */

/**
 * Add a content type to a given workflow
 *
 * @param String $content_type
 *   Machine name of the content type.
 * @param String $workflow
 *   Name of the workflow.
 * @param Array $placement
 *   An array of one of the following:
 *     array('node')
 *     array('comment')
 *     array('node', 'comment')
 * @return Boolean
 *   TRUE if the content type was successfully associated with the content
 *   type, FALSE when unsuccessfull.
 */
function dt_workflow_set_content_type_workflow($content_type, $workflow_name, $placement = array('node')) {
  $wid = dt_workflow_get_workflow_wid($workflow_name);
  if (!$wid) {
    // If the workflow doesn't exist, don't continue
    return FALSE;
  }

  $workflow_type = db_fetch_object(db_query("SELECT * FROM {workflow_type_map} WHERE type = '%s'", $content_type));
  if ($workflow_type->type) {
    $exists = TRUE;
  }

  if ($exists) {
    db_query("UPDATE {workflow_type_map} SET wid = %d WHERE type = '%s'", $wid, $content_type);
  }
  else {
    db_query("INSERT INTO {workflow_type_map} (type, wid) VALUES ('%s', %d)", $content_type, $wid);
  }
  variable_set('workflow_'. $content_type, $placement);

  // Invoke hook_dt_workflow_set_content_type_workflow().
  module_invoke_all('dt_workflow_set_content_type_workflow', $content_type, $workflow_name, $wid);

  return TRUE;
}

/**
 * Create a new workflow
 *
 * Running workflow_create() can fail inexplicably in rare instances.
 *
 * @param String $workflow_name
 *   User-friendly name of the workflow to create
 * @param Array $states
 *   An array of user-friendly names of states that should be assigned to this
 *   workflow.
 * @return Numeric $wid
 *   The WID of the newly created workflow
 */
function dt_workflow_create_workflow($workflow_name, $states = array()) {
  $options = serialize(array('comment_log_node' => 1, 'comment_log_tab' => 1));
  db_query("INSERT INTO {workflows} (name, options) VALUES ('%s', '%s')", $workflow_name, $options);
  $wid = $mlid = db_last_insert_id('workflows', 'wid');
  workflow_state_save(array(
    'wid' => $wid,
    'state' => t('(creation)'),
    'sysid' => WORKFLOW_CREATION,
    'weight' => WORKFLOW_CREATION_DEFAULT_WEIGHT,
  ));
  menu_rebuild();

  // Add the states
  if (is_array($states)) {
    foreach ($states as $weight => $state) {
      dt_workflow_add_state($state, $workflow_name, $weight);
    }
  }

  // Invoke hook_dt_workflow_create_workflow().
  module_invoke_all('dt_workflow_create_workflow', $workflow_name);

  return $wid;
}

/**
 * Delete a workflow by its name
 *
 * @param String $workflow_name
 *   User-friendly name of the workflow to delete.
 */
function dt_workflow_delete_workflow($workflow_name) {
  $wid = dt_workflow_get_workflow_wid($workflow_name);

  // Gather all of its states
  $result = db_query('SELECT * FROM {workflow_states} WHERE wid = %d', $wid);
  $workflow_states = array();
  while ($data = db_fetch_object($result)) {
    $workflow_states[] = $data->state;
  }

  // Now delete the workflow itself
  workflow_deletewf($wid);

  // Since workflow_deletewf() leaves reminents, really delete its states
  foreach ($workflow_states as $state_name) {
    dt_workflow_delete_state($state_name, $workflow_name);
  }
}

/**
 * Delete a state by its name
 *
 * @param String $state_name
 *   User-friendly name of the state to delete.
 * @param String $workflow_name
 *   User-friendly name of the workflow that the state is associated with.
 *   Provide this in cases where there are more than two states with the same
 *   user-friendly name.
 */
function dt_workflow_delete_state($state_name, $workflow_name = NULL) {
  $sid = dt_workflow_get_state_sid($state_name, $workflow_name);

  // Delete the state
  workflow_state_delete($sid);

  // workflow_state_delete() only disables the state.  Delete it for realz.
  db_query("DELETE FROM {workflow_states} WHERE sid = %d", $sid);
}

/**
 * Get the wid of a given workflow name
 *
 * @param String $workflow_name
 *   User-friendly name of the state
 * @return Numeric
 *   WID of the requested workflow
 */
function dt_workflow_get_workflow_wid($workflow_name) {
  $wid = db_result(db_query("SELECT wid FROM {workflows} WHERE name = '%s'", $workflow_name));
  return $wid;
}

/**
 * Get the sid of a given state name
 *
 * @param String $state_name
 *   User-friendly name of the state
 * @param String $workflow_name
 *   User-friendly name of the workflow that the state is associated with.
 *   Provide this in cases where there are more than two states with the same
 *   user-friendly name.
 * @return Numeric
 *   SID of the requested state
 */
function dt_workflow_get_state_sid($state_name, $workflow_name = NULL) {
  if ($workflow_name) {
    $wid = dt_workflow_get_workflow_wid($workflow_name);
    $sid = db_result(db_query("SELECT sid FROM {workflow_states} WHERE state = '%s' AND wid = %d", $state_name, $wid));
  }
  else {
    $sid = db_result(db_query("SELECT sid FROM {workflow_states} WHERE state = '%s'", $state_name));
  }
  return $sid;
}

/**
 * Add state to workflow
 *
 * Running workflow_state_save() can fail inexplicably in rare instances.
 *
 * @param String $state_name
 *   User-friendly name of the state
 * @param String $workflow_name
 *   User-friendly name of the workflow that the state is associated with.
 *   Provide this in cases where there are more than two states with the same
 *   user-friendly name.
 * @param Numeric $weight
 *   Weight of the state
 * @param Array $options
 *   Additional options to be passed through to the invoked hook.
 * @return Numeric
 *   SID of the newly created state
 */
function dt_workflow_add_state($state_name, $workflow_name, $weight = 0, $options = array()) {
  $wid = dt_workflow_get_workflow_wid($workflow_name);

  db_query("INSERT INTO {workflow_states} (wid, state, weight, sysid, status) " .
      "VALUES (%d, '%s', %d, 0, 1)", $wid, $state_name, $weight);
  $sid = db_last_insert_id('workflow_states', 'sid');

  // Invoke hook_dt_workflow_add_state().
  module_invoke_all('dt_workflow_add_state', $state_name, $sid, $workflow_name, $weight, $options);

  return $sid;
}

/**
 * Configure a role's access to a given state
 *
 * @param Numeric $sid
 *   State id
 * @param Numeric $rid
 *   Role ID
 * @param Boolean $view
 *   1 to grant view access, 0 to disallow
 * @param Boolean $update
 *   1 to grant update access, 0 to disallow
 * @param Boolean $delete
 *   1 to grant delete access, 0 to disallow
 */
function dt_workflow_add_role_access($sid, $rid, $view, $update, $delete) {
  $exist = db_result(db_query("SELECT sid FROM {workflow_access} WHERE sid = %d AND rid = %d", $sid, $rid));
  if ($exist) {
    db_query("UPDATE {workflow_access} SET grant_view = %d, grant_update = %d, grant_delete = %d WHERE sid = %d AND rid = %d", $view, $update, $delete, $sid, $rid);
  }
  else {
    db_query("INSERT INTO {workflow_access} (sid, rid, grant_view, grant_update, grant_delete) VALUES (%d, %d, %d, %d, %d)", $sid, $rid, $view, $update, $delete);
  }
}

/**
 * Add a role to the list of those allowed for a given transition.
 *
 * Add the transition if necessary.
 *
 * Running workflow_transition_add_role() can fail inexplicably in rare instances.
 *
 * @param int $from
 * @param int $to
 * @param mixed $role
 *   Int (role ID) or string ('author')
 * @return Numeric
 *   The ID of the transition between the two states given
 */
function dt_workflow_transition_add_role($from, $to, $role) {
  $transition = array(
    'sid' => $from,
    'target_sid' => $to,
    'roles' => $role,
  );
  $tid = workflow_get_transition_id($from, $to);
  if ($tid) {
    $roles = db_result(db_query("SELECT roles FROM {workflow_transitions} WHERE tid = %d", $tid));
    $roles = explode(',', $roles);
    if (array_search($role, $roles) === FALSE) {
      $roles[] = $role;
      $transition['roles'] = implode(',', $roles);
      $transition['tid'] = $tid;
      db_query("UPDATE {workflow_transitions} " .
          "SET sid = %d, target_sid = %d, roles = '%s'" .
          "WHERE tid = %d", $transition['sid'], $transition['target_sid'], $transition['roles'], $tid);
    }
  }
  else {
    db_query("INSERT INTO {workflow_transitions} (sid, target_sid, roles) " .
        "VALUES (%d, %d, '%s')", $transition['sid'], $transition['target_sid'], $transition['roles']);
    $tid = db_last_insert_id('workflow_transitions', 'tid');
  }

  return $tid;
}

/**
 * Get the workflow transition information for a given state
 *
 * @param Numeric $sid
 *   The SID of the source state
 * @return Array
 *   A list of transitions keyed by the target state ID
 */
function dt_workflow_get_transition($sid) {
  $query = db_query("SELECT * FROM {workflow_transitions} WHERE sid = %d", $sid);
  $transitions = array();
  while ($transition = db_fetch_object($query)) {
    $transitions[$transition->target_sid] = $transition;
  }
  return $transitions;
}

/**
 * Get the workflow access information for a given state
 *
 * @param Numeric $sid
 *   The SID of the state
 * @param Numeric $rid
 *   Optionally provide the role id to limit which access information is returned
 * @return Array
 *   A list of all access information keyed by the role ID
 */
function dt_workflow_get_access($sid, $rid = FALSE) {
  if ($rid) {
    $query = db_query("SELECT * FROM {workflow_access} WHERE sid = %d AND rid = %d", $sid, $rid);
  }
  else {
    $query = db_query("SELECT * FROM {workflow_access} WHERE sid = %d", $sid);
  }
  $accesses = array();
  while ($access = db_fetch_object($query)) {
    $accesses[$access->rid] = $access;
  }
  return $accesses;
}

/**
 * Assign an action to a workflow transition
 *
 * I know I promised Tyler I would use drupal_execute() on my next API.  I'm
 * just currently intimidated with the idea of pre-generating the form's values
 * and only altering the one I want.  This form is particularly large and
 * overwhelming, though.  I will try it some day.
 *
 * @param String $content_type
 *   Machine name of the content type
 * @param String $source_state
 *   User-friendly name of the source state
 * @param String $target_state
 *   User-friendly name of the target state
 * @param String $aid
 *   The action id, which apparently can be a string.
 * @param Integer $weight
 *   Numeric value of the weight -- which actions should be done in what order
 * @param String $workflow_name
 *   User-friendly name of the workflow
 */
function dt_workflow_assign_action($content_type, $source_state, $target_state, $aid, $weight, $workflow_name = NULL) {
  // Calculate the transition id
  $source_sid = dt_workflow_get_state_sid($source_state, $workflow_name);
  $target_sid = dt_workflow_get_state_sid($target_state, $workflow_name);
  $tid = workflow_get_transition_id($source_sid, $target_sid);

  // Generate trigger's values
  $trigger_hook = 'workflow';
  $trigger_op = 'workflow-'. $content_type .'-'. $tid;

  // Delete existing entry
  db_query("DELETE FROM {trigger_assignments} WHERE hook = '%s' AND op = '%s' AND aid = '%s'", $trigger_hook, $trigger_op, $aid);

  // Insert the entry into the database
  db_query("INSERT INTO {trigger_assignments} VALUES ('%s', '%s', '%s', %d)", $trigger_hook, $trigger_op, $aid, $weight + 1);
}

/**
 * Get the actions associated with a given workflow transition
 *
 * @param String $content_type
 *   Machine name of the content type
 * @param String $source_state
 *   User-friendly name of the source state
 * @param String $target_state
 *   User-friendly name of the target state
 * @param String $workflow_name
 *   User-friendly name of the workflow
 * @return Array
 *   A list of action IDs associated with the given transition and content type
 */
function dt_workflow_get_transition_actions($content_type, $source_state, $target_state, $workflow_name = NULL) {
  // Calculate the transition id
  $source_sid = dt_workflow_get_state_sid($source_state, $workflow_name);
  $target_sid = dt_workflow_get_state_sid($target_state, $workflow_name);
  $tid = workflow_get_transition_id($source_sid, $target_sid);

  // Generate trigger's values
  $trigger_hook = 'workflow';
  $trigger_op = 'workflow-'. $content_type .'-'. $tid;

  // Look for these values in the database
  $query = db_query("SELECT * FROM {trigger_assignments} WHERE hook = '%s' AND op = '%s'", $trigger_hook, $trigger_op);
  $actions = array();
  while ($data = db_fetch_object($query)) {
    $actions[] = $data->aid;
  }

  return $actions;
}
