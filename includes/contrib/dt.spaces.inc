<?php

/**
 * @file Expanded API for spaces.module
 */

/**
 * Enable a feature in override spaces according to preset.
 */
function dt_spaces_og_edit_feature($feature, $presets = array(), $types = array(), $enable = 1) {
  $batch = array(
    'title' => t('Rebuilding content access permissions'),
    'operations' => array(
      array('_dt_spaces_og_edit_feature', array($feature, $presets, $types, $enable)),
    ),
  );
  batch_set($batch);
}

/**
 * Batch callback that loops through all group nodes
 */
function _dt_spaces_og_edit_feature($feature, $presets, $types, $enable, &$context) {
  if (empty($context['sandbox'])) {
    // Initiate multistep processing.
    $nids = array();
    if (empty($types)) {
      $types = array();
      foreach (node_get_types() as $key => $info) {
        if (og_is_group_type($key)) {
          $types[] = $key;
        }
      }
    }
    if ($types) {
      $args = $types;
      $sql = 'SELECT n.nid from {node} n '
            . 'INNER JOIN {spaces_overrides} so on so.type = "og" AND so.id = n.nid AND '
            . 'so.object_type = "variable" AND so.object_id = "spaces_features" ';
      $where = 'WHERE n.type ';
      $where .= (count($types) == 1 ? '= "%s" ' : 'in (' . db_placeholders($types, 'varchar'). ') ');
      if ($presets) {
        $sql .= 'INNER JOIN {spaces_overrides} so2 on so2.type = "og" AND so2.id = nid AND '
             . 'so2.object_type = "variable" AND so2.object_id = "spaces_preset_og" ';
        $where .= ' AND so2.value ';
        $where .= (count($presets) == 1? ' = "%s"' : 'in (' . db_placeholders($presets, 'varchar') . ')');
        foreach ($presets as $preset) {
          $args[] = serialize($preset);
        }
      }
      $result = db_query($sql . $where, $args);
      while ($row = db_fetch_object($result)) {
        $nids[$row->nid] = $row->nid;
      }
    }
    else {
      // No og group types!
      return;
    }
    $context['sandbox']['max'] = count($nids);
    $context['sandbox']['nids'] = $nids;
  }
  $count = 0;
  while (($nid = array_shift($context['sandbox']['nids'])) && $count < 100) {
    $count++;
    if ($space = spaces_load('og', $nid)) {
      // Check if this is a space that applies
      $features_space = $space->controllers->variable->get('spaces_features', 'space');
      $features_original = $space->controllers->variable->get('spaces_features', 'preset');
      // Only alter if not set to current value.
      if (!isset($features_space[$feature]) || $features_space[$feature] != $enable) {
        $features_space[$feature] = $enable;
        $space->controllers->variable->set('spaces_features', $features_space);
      }
    }
  }
  if (!empty($context['sandbox']['nids'])) {
    $context['finished'] = ($context['sandbox']['max'] - count($context['sandbox']['nids'])) / $context['sandbox']['max'];
  }
}

function dt_spaces_verify_space_type_ready($type = 'og') {
  $registry = spaces_types();
  if (empty($registry[$type])) {
    // try to clear cache.
    $registry = spaces_types(TRUE);
    if (empty($registry[$type])) {
      return FALSE;
    }
  }
  ctools_include('plugins');
  $plugins = ctools_get_plugins('spaces', 'plugins');
  $info = $registry[$type];
  if (!isset($plugins[$info['plugin']])) {
    ctools_static('ctools_plugin_info', array(), TRUE);
    ctools_static('ctools_plugins', array(), TRUE);
    $plugins = ctools_get_plugins('spaces', 'plugins');
  }
  return isset($plugins[$info['plugin']]);
}

/*
 * Returns whether a given feature is enabled in a given space.
 */
function dt_spaces_feature_is_enabled($feature, $space = NULL) {
  if (!isset($space)) {
    $space = spaces_get_space();
  }
  if ($space) {
    $space_features = $space->controllers->variable->get('spaces_features');
    if (!empty($space_features)) {
      $features = array_keys(array_filter($space_features));
      return in_array($feature, $features);
    }
  }
  return FALSE;
}

/**
 * Active a space from node (or de-active)
 */
function dt_spaces_toggle_space_from_node($node = NULL, $active = TRUE) {
  global $conf, $user;
  static $conf_save, $old_space = FALSE, $roles;
  if ($active && $node) {
    if ($space = spaces_load('og', $node->nid)) {
      $roles = $user->roles;
      $old_space = spaces_get_space();
      if ($old_space->id == $space->id) {
        return;
      }
      $conf_save = $conf;
      if (!empty($node->spaces_preset_og) && $node->spaces_preset_og !== $space->controllers->variable->get('spaces_preset_og')) {
        $space->controllers->variable->set('spaces_preset_og', $node->spaces_preset_og);
        $space = spaces_load('og', $node->nid, TRUE);
      }
      $space->init_overrides();
      og_get_subscriptions($user->uid, 1, TRUE);
      spaces_set_space($space);
      og_set_group_context($space->group);
      module_invoke('dt_spaces_toggle_space', 'activate', $space);

      // Support user roles.
      if (module_exists('og_user_roles')) {
        if ($user->site_roles) {
          $user->roles = $user->site_roles;
        }
        og_user_roles_grant_roles($user, $space->group);
      }
    }
  }
  elseif (!$active && isset($conf_save)) {
    spaces_set_space($old_space);
    $group = !empty($old_space) && !empty($old_space->group) ? $old_space->group : FALSE;
    og_set_group_context($group);
    $conf = $conf_save;
    module_invoke('dt_spaces_toggle_space', 'deactivate', $space);
    $user->roles = $roles;
  }
}

/**
 * Sets variable for current space.
 */
function dt_spaces_variable_set($name, $val) {
  if ($space = spaces_get_space()) {
    $space->controllers->variable->set($name, $val);
    global $conf;
    $conf[$name] = $val;
  }
  else {
    variable_set($name, $val);
  }
}
