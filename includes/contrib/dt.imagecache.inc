<?php

/**
 * @file Expanded API for imagecache.module
 */

/**
 * Return specific information about an imagecache preset
 * (originally copied from dcss/dcss.inc)
 *
 * @param String preset
 *   A preset name.
 * @param String Type
 *   what information to return.
 * @param reset
 *   if set to TRUE it will clear the preset cache
 * @param aspect_ratio Float
 *   if set, opposite percentage dimension will be set as ratio of given
 * @param append_px
 *   if set to FALSE will not append "px" to values
 *
 * @return
 *   array of presets array( $preset_id => array('presetid' => integer, 'presetname' => string))
 */
function dt_imagecache_get_preset($preset, $type = '', $reset = FALSE, $aspect_ratio = NULL, $append_px = TRUE) {
  static $info = array();
  $preset = dt_imagecache_preset($preset);
  $dimensions = array('width', 'height');
  if (empty($info[$preset]) || !empty($reset)) {
    // this is cached data
    $presets = imagecache_presets();
    $info[$preset]['width'] = 0;
    $info[$preset]['height'] = 0;
    // go through each action
    foreach ($presets[$preset]['actions'] as $action) {
      // go through each dimension
      foreach ($dimensions as $dimension_index => $dimension) {
        // if dimension is set, see if should add it
        if (!empty($action['data'][$dimension])) {
          // if is a percent, and there's already a dimension set, apply it
          if (strpos($action['data'][$dimension], '%') !== FALSE) {
            if (!empty($action['data'][$dimension])) {
              if (!empty($aspect_ratio)) {
                // Calculate the opposite dimension using the given aspect_ratio
                $info[$preset][$dimension] = round($info[$preset][$dimensions[(int)!(bool)$dimension_index]] * $aspect_ratio);
              }
              else {
                $info[$preset][$dimension] = _imagecache_percent_filter($action['data'][$dimension], $info[$preset][$dimension]);
              }
            }
          }
          else {
            // set the preset
            $info[$preset][$dimension] = $action['data'][$dimension];
          }
        }
      }
    }

    if ($append_px){
      $info[$preset]['width'].= 'px';
      $info[$preset]['height'].= 'px';
    }
    $info[$preset]['both'] = $info[$preset]['width'] .' '. $info[$preset]['height'];
  }
  if (!empty($type)) {
    return (!empty($info[$preset][$type])?$info[$preset][$type]:0);
  }
  else {
    return $info[$preset];
  }
}

/**
 * Allow dynamic renaming of imagecache; this is just logic, relavent areas
 * Need to be adjusted to use
 */
function dt_imagecache_preset($presetname) {
  global $theme_key;
  if (imagecache_preset_by_name($theme_key . '_' . $presetname)) {
    return $theme_key . '_' . $presetname;
  }
  return $presetname;
}
