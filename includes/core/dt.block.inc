<?php

/**
 * @file Expanded API for blog.module
 */

/**
 * Modifies a given block
 *
 * @param String $action
 *   MySQL compatible statements that follow the SET command.
 * @param String $module
 *   Name of the module
 * @param String $delta
 *   Delta of the specific block within the module
 * @param Array $themes
 *   An array of themes you want the modifications to target.  Leave FALSE to
 *   target all enabled themes.
 * @return
 *   An array of block objects, one for each theme
 */
function dt_block_modify($action, $module, $delta, $themes = FALSE) {
  // If the block doesn't exist, create an entry for it
  $block = dt_block_get($module, $delta, $themes);
  if (!isset($block)) {
    dt_block_add($module, $delta, '', $themes);
  }

  // Update the block with the actions
  if (is_array($themes)) {
    foreach ($themes as $theme) {
      $sql = 'UPDATE {blocks} SET '. $action .' WHERE module = "%s" AND delta = "%s" AND theme = "%s"';
      db_query($sql, $module, $delta, $theme);
    }
  }
  else {
    $sql = 'UPDATE {blocks} SET '. $action .' WHERE module = "%s" AND delta = "%s"';
    db_query($sql, $module, $delta);
  }

  return dt_block_get($module, $delta, $themes);
}

/**
 * Adds the database entry for the block
 *
 * a database entry is only created for a block when the block page is loaded,
 * which prevents the ability to configure a block's title or other settings.
 *
 * @param String $module
 *   Name of the module
 * @param String $delta
 *   Delta of the specific block within the module
 * @param String $region
 *   Region the block should be inserted into. Compensates for primary/left
 *   secondary/right regions.  State 'primary' if you mean 'left'.
 * @param Array $themes
 *   An array of themes you want the modifications to target.  Leave FALSE to
 *   target all enabled themes.
 * @return
 *   An array of block objects, one for each theme
 */
function dt_block_add($module, $delta, $region = '', $themes = FALSE) {
  // A block needs to be attached to a theme.  Grabbing all enabled themes if none specified
  $themes = is_array($themes) ? $themes : dt_theme_enabled_themes();

  foreach ($themes as $theme) {
    db_query("INSERT INTO {blocks} (module, delta, theme, region) " .
        "VALUES ('%s', '%s', '%s', '%s')", $module, $delta, $theme, $region);
  }

  return dt_block_get($module, $delta, $themes);
}

/**
 * Deletes a block.  This is mostly for unit tests.
 *
 * Most blocks are created in code, so this will not actually delete a block
 * completely from a system.
 *
 * @param String $module
 *   Name of the module
 * @param String $delta
 *   Delta of the specific block within the module
 * @param Array $themes
 *   An array of themes you want the modifications to target.  Leave FALSE to
 *   target all enabled themes.
 * @return
 *   If the delete was successful, it should return FALSE, else it will return
 *   the block object.
 */
function dt_block_delete($module, $delta, $themes = FALSE) {
  // A block needs to be attached to a theme.  Grabbing all enabled themes if none specified
  $all_themes = list_themes();

  foreach ($all_themes as $theme) {
    db_query("DELETE FROM {blocks} WHERE module = '%s' AND delta = '%s' AND theme = '%s'", $module, $delta, $theme->name);
  }

  return dt_block_get($module, $delta, $themes);
}

/**
 * Gets a block object from the database
 *
 * @param String $module
 *   Name of the module
 * @param String $delta
 *   Delta of the specific block within the module
 * @param Array $themes
 *   An array of themes you want the modifications to target.  Leave FALSE to
 *   target all enabled themes.
 * @return
 *   block object, in an array by its theme
 */
function dt_block_get($module, $delta, $themes = FALSE) {
  $themes = is_array($themes) ? $themes : dt_theme_enabled_themes();

  $query = db_query("SELECT * FROM {blocks} WHERE module = '%s' AND delta = '%s'", $module, $delta);
  while ($block = db_fetch_object($query)) {
    if (in_array($block->theme, $themes)) {
      $results[$block->theme] = $block;
    }
  }

  return $results;
}
