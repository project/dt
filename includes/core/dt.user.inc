<?php

/**
 * @file Expanded API for user.module
 */

/**
 * Call user_load and cache the loaded user.
 *
 * @param Array $array
 *   The machine-readable name of the menu.
 * @param String $reset
 *   Resets the loaded user for that uid
 * @return
 *   The the loaded.
 */
function dt_user_load($array, $reset = FALSE) {
  static $accounts;
  if (is_numeric($array) && isset($accounts[$array]) && empty($reset)) {
    return $accounts[$array];
  }
  else {
    $account = user_load($array);
    if (isset($account->uid)) {
      $accounts[$account->uid] = $account;
    }
    return $account;
  }
}

/**
 * Logout the current user.
 */
function dt_user_logout() {
  global $user;
  if (!empty($user->uid)) {
    watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

    // Destroy the current session:
    session_destroy();
    // Only variables can be passed by reference workaround.
    $null = NULL;
    user_module_invoke('logout', $null, $user);

    // Load the anonymous user
    $user = drupal_anonymous_user();
  }
}

/**
 * @defgroup dt_user_role Expanded API for managing roles
 * @{
 */

/**
 * Creates a role with the given name
 *
 * @param String $role_name
 *   User friendly name of the role
 * @param Array $options
 *   An array of options to be passed to the creation hook
 * @return Integer
 *   rid of the newly created role
 */
function dt_user_role_create_role($role_name, $options = array()) {
  $rid = dt_user_role_get_rid($role_name);

  if (empty($rid)) {
    db_query("INSERT INTO {role} (name) VALUES ('%s')", $role_name);
    $rid = db_last_insert_id('role', 'rid');

    // Invokes hook_dt_user_role_create_role().
    module_invoke_all('dt_user_role_create_role', 'create', $role_name, $rid, $options);
  }
  else {
    // Even if the role already exists, we still want to invoke this hook in case
    // the $options associated with the role may be different
    // Invokes hook_dt_user_role_create_role().
    module_invoke_all('dt_user_role_create_role', 'update', $role_name, $rid, $options);
  }

  return $rid;
}

/**
 * Retrives the rid of a given role
 *
 * @param String $role_name
 *   User friendly name of the role
 * @return Integer
 *   rid of the requested role
 */
function dt_user_role_get_rid($role_name, $reset = FALSE) {
  static $all_roles;
  $reset = !$reset ? variable_get('phpunit_tests', FALSE) : $reset;
  if (!isset($all_roles) || !in_array($role_name, $all_roles) || $reset) {
    $all_roles = user_roles();
  }
  foreach ($all_roles as $rid => $role) {
    if ($role == $role_name) {
      return ($rid);
    }
  }
}

/**
 * Rename a role's user friendly name
 *
 * @param String $original_name
 *   The original name of the role
 * @param String $new_name
 *   The new name the role should become
 * @param Integer $rid
 *   Optionally provide the specific role id to rename
 */
function dt_user_role_rename($original_name, $new_name, $rid = FALSE) {
  // First make sure the role doesn't already exist; delete if so.
  dt_user_role_remove_role($new_name);
  $sql = "UPDATE {role} SET name = '%s' WHERE name = '%s'";
  if (is_numeric($rid)) {
    $sql .= " AND rid = %d";
  }
  db_query($sql, $new_name, $original_name, $rid);
}

/**
 * Removes the given role
 *
 * @param String $role_name
 *   User friendly name of the role
 * @param Array $options
 *   An array of options to be passed to the removal hook
 */
function dt_user_role_remove_role($role_name, $options = array()) {
  $rid = dt_user_role_get_rid($role_name);

  if (!empty($rid) && is_numeric($rid)) {
    // taken from user_admin_role_submit
    db_query('DELETE FROM {role} WHERE rid = %d', $rid);
    db_query('DELETE FROM {permission} WHERE rid = %d', $rid);

    // Update the users who have this role set:
    db_query('DELETE FROM {users_roles} WHERE rid = %d', $rid);

    // Invoke hook_dt_user_role_remove_role().
    module_invoke_all('dt_user_role_remove_role', $role_name, $rid, $options);
  }
}

/**
 * Merge the given role into another
 *
 * @param String $old_role_name
 *   The role to merge and remove.
 * @param String $new_role_name
 *   The existing role to merge in.
 * @param Array $options
 *   An array of options to be passed to the removal hook
 */
function dt_user_role_merge_role($old_role_name, $new_role_name) {
  $old_rid = dt_user_role_get_rid($old_role_name);
  $new_rid = dt_user_role_get_rid($new_role_name, TRUE);

  if ($old_rid && $new_rid) {
    // Need two queiries to avoid duplicates where user has both roles.
    // Don't think the IGNORE syntex is considered standard sql.
    $insert_query = "INSERT INTO {users_roles} (uid, rid) SELECT uid, %d FROM {users_roles} "
      ."WHERE rid = %d AND uid NOT IN (SELECT uid FROM {users_roles} WHERE rid = %d)";
    db_query($insert_query, $new_rid, $old_rid, $new_rid);
    db_query("DELETE FROM {users_roles} WHERE rid = %d", $old_rid);

    // Og user roles support.
    if (db_table_exists('og_users_roles')) {
      // TODO do this query in such a way that it is effiecent, doesn't duplicate.
      $insert_query = "INSERT IGNORE INTO {og_users_roles} (gid, uid, rid) SELECT gid, uid, %d FROM {og_users_roles} WHERE rid = %d";
      db_query($insert_query, $new_rid, $old_rid);
      db_query("DELETE FROM {og_users_roles} WHERE rid = %d", $old_rid);
    }

    // Delete the old role
    dt_user_role_remove_role($old_role_name, $old_rid);
    module_invoke_all('dt_user_role_merge_role', $old_rid, $new_rid);
  }
}

/**
 * Assign a role to a user
 *
 * @param Array $new_roles
 *   User friendly name of the role
 * @param String $username
 *   Username of the user to manipulate
 * @param Boolean $preserve_existing
 *   Should the configured $roles be additive to existing roles, or replace
 *   all existing roles.
 */
function dt_user_role_assign_roles($new_roles, $username, $preserve_existing = TRUE) {
  $account = user_load(array('name' => $username));

  $roles = array();
  if ($preserve_existing) {
    $roles = $account->roles;
  }
  else {
    // Make sure the authenticated user role isn't lost
    $roles[2] = 'authenticated user';
  }
  foreach ($new_roles as $role_name) {
    $rid = dt_user_role_get_rid($role_name);
    $roles[$rid] = $role_name;
  }

  return user_save($account, array('roles' => $roles));
}

/**
 * Return all default roles
 *
 * @param String $module
 *  The module that defined the roles.
 * @return Array
 *   All roles and their options
 */
function dt_user_role_default_roles($module = 'all', $reset = FALSE) {
  static $roles;
  $reset = !$reset ? variable_get('phpunit_tests', FALSE) : $reset;
  if (!isset($roles) || $reset) {
    $_roles = dt_cached_hook_modify('dt_user_role_roles', NULL, $reset);
    $roles = array(
      'all' => array(),
    );
    foreach ($_roles as $key => $value) {
      if (!isset($roles[$value['module']])) {
        $roles[$value['module']] = array();
      }
      $roles[$value['module']][$key] = $value;
      $roles['all'][$key] = $value;
    }
  }
  return isset($roles[$module])? $roles[$module] : array();
}

/**
 * Return all default roles by type
 *
 * @param String $type
 *   The type of roles to return.  Use 'site' for all site-wide roles, or 'og'
 *   for og specific roles.  If none provided, then all roles returned.
 * @return Array
 *   All roles and their options
 */
function dt_user_role_default_roles_by_type($type = 'site', $space_preset = NULL, $reset = FALSE) {
  static $roles = array();
  if ($space_preset) {
    $key = $type .'_'. $space_preset;
  }
  else {
    $key = $type;
  }
  if (!isset($roles[$key]) || $reset) {
    $roles[$key] = array();
    $all_roles = dt_user_role_default_roles('all', $reset);
    foreach ($all_roles as $role_name => $role) {
      if (
        (empty($role['og_reserve_roles']) && $type == 'site')
        || (!empty($role['og_reserve_roles']) && $type == 'og'
        && (empty($space_preset) || $role['space_preset'] == $space_preset))
        ) {
        $roles[$key][$role_name] = $role;
      }
    }
  }
  return $roles[$key];
}

/**
 * Updates all default roles
 */
function dt_user_role_update_default_roles($module = 'all', $reset = FALSE) {
  $roles = dt_user_role_default_roles($module, $reset);
  ksort($roles);
  foreach ($roles as $role_name => $options) {
    dt_user_role_create_role($role_name, $options);
  }
  dt_user_role_default_roles('all', TRUE);
}

/**
 * Delete all default roles
 */
function dt_user_role_delete_default_roles($module = 'all', $reset = FALSE) {
  $roles = dt_user_role_default_roles($module, $reset);
  foreach ($roles as $role_name => $options) {
    dt_user_role_remove_role($role_name, $options);
  }
  dt_user_role_default_roles('all', TRUE);
}

/**
 * Caching function to replace user_roles(), since it does not cache itself.
 *
 * Using this helps avoid duplicate queries. However, it can only replace where
 * user_roles() is called without parameters.
 */
function dt_user_role_roles() {
  static $cache;
  if (!isset($cache)) {
    $cache = user_roles();
  }
  return $cache;
}

/**
 * @} End of "defgroup dt_user_role".
 */


/**
 * @defgroup dt_hooks_user Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_dt_module_install().
 */
function dt_user_dt_module_install($module, $module_info) {
  // Automatically create any roles defined in hook_dt_user_role_roles().
  dt_user_role_update_default_roles($module, TRUE);
}

/**
 * @} End of "defgroup dt_hooks_user".
 */
