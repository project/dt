<?php

/**
 * @file Expanded API for menu.module
 */

/**
 * Returns information about the menu
 *
 * @param unknown_type $menu_name
 * @param unknown_type $item
 */
function dt_menu_tree_page_data($menu_name = 'primary-links', $item = NULL) {
  if ($item) {
    $item_save = menu_get_item();
    menu_set_item(NULL, $item);
  }
  $tree = menu_tree_page_data($menu_name);
  if ($item) {
    menu_set_item(NULL, $item_save);
  }
  return $tree;
}
