<?php

/**
 * @file Expanded API for node.module
 */

/**
 * Figures out if is a node form
 *
 * @param Array $form
 *   the form array
 * @param Array types
 *   An array of types to check for
 */

function dt_is_node_form($form, $types = array()) {
  if (is_string($types)) {
    $types = array($types);
  }
  $form_id = $form['form_id']['#value'];
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] .'_node_form' == $form_id) {
    if (empty($types) || in_array($form['type']['#value'], $types)) {
      return $form['type']['#value'];
    }
  }
  return FALSE;
}

/**
 * Returns array of node types.
 * Use in basic hooks to prevent node_get_types static cache-ing
 * Ie. hook_strongarm_alter.
 */
function dt_node_get_types($reset = FALSE) {
  static $types;
  global $dt_node_types_needs_rebuilds;

  if (!empty($reset) || !empty($dt_node_types_needs_rebuilds) || !isset($types)) {
    list($_node_types, $_node_names) = _node_types_build();
    $types = drupal_map_assoc(array_keys($_node_types));
  }

  if ($dt_node_types_needs_rebuilds === TRUE) {
    // This is set by dt_node_info() (a hook_node_info()
    // implementation) in dt.module. It basically tells us that we should
    // flush the cache of this function the next time it is run. Note that just
    // calling dt_node_get_types(FALSE) from that function will cause an
    // infinite loop, because this function calls _node_types_build(), which
    // calls hook_node_info() implementations.
    $dt_node_types_needs_rebuilds = FALSE;
  }
  return $types;
}

/**
 * Wrapper around node_access
 *
 * Should be called via
 * $node_access = function_exists('dt_node_access') ? 'dt_node_access' : 'node_access';
 * if ($node_access('create', 'blah')) {}
 *
 * Method does not work for $account, so not bothering with account.
 */
function dt_node_access($op, $node) {
  switch ($op) {
    case 'create':
      $path = 'node/add/' . str_replace('_', '-', $node);
      break;
    case 'update':
      $path = 'node/' . $node->nid  . '/edit';
      break;
    case 'delete':
      $path = 'node/' . $node->nid  . '/delete';
      break;
  }
  if (!empty($path) && ($item = menu_get_item($path)) && empty($item['access'])) {
    return FALSE;
  }
  return node_access($op, $node);
}

/**
 * Creates a begining node.
 *
 * @param String $type
 *   The node type to create
 * @param Array $values
 *   Initial values for the node.
 */
function dt_node_prepare($type, $values = array()) {
  static $default_values = array();
  $node = (object)$values;
  $node->type = $type;
  // Set the author if not set.
  if (!isset($node->uid)) {
    if (isset($node->name)) {
      $account = user_load(array('name' => $node->name));
    }
    else {
      global $user;
      $account = $user;
    }
    $node->uid = $account->uid;
    $node->name = $account->name;
  }
  else {
    // Make sure name is set
    $account = dt_user_load($node->uid);
    $node->name = $account->name;
  }

  // Find the default values for the node typee.
  if (!isset($default_values[$type])) {
    $default_values[$type] = array();
    $info = content_types($type);
    // content default value only cares about $field_info
    $form = array();
    $form_state = array();
    foreach($info['fields'] as $field_name => $field_info) {
      $default_values[$type][$field_name] = content_default_value($form, $form_state, $field_info, 0);
    }
  }

  // Set the default values
  foreach($default_values[$type] as $field_name => $default) {
    if (!isset($node->$field_name)) {
      $node->$field_name = $default;
    }
  }
  // Allow for setting up the node
  node_invoke($node, 'prepare');
  node_invoke($node, 'dt_prepare');
  node_submit($node);
  // Set lost values
  if (!empty($values['created'])) {
    $node->created = $values['created'];
  }
  return $node;
}

/**
 * Tracks whether a certian node has been loaded yet.
 */
function dt_node_loaded($nid, $loaded = NULL) {
  static $nids = array();
  if ($loaded) {
    $nids[$nid] = TRUE;
  }
  return !empty($nids[$nid]);
}


/**
 * @defgroup dt_hooks_node Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_node_info().
 */
function dt_node_node_info() {
  global $dt_node_types_needs_rebuilds;
  $dt_node_types_needs_rebuilds = TRUE;
  return array();
}

/**
 * Implementation of hook_nodeapi().
 */
function dt_node_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'load':
      // Provides tracking whether a node has been loaded
      dt_node_loaded($node->nid, TRUE);
      break;
  }
}

/**
 * @} End of "defgroup dt_hooks_node".
 */
