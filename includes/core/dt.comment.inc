<?php

/**
 * @file Expanded API for comment.module
 */

/**
 * Define a loader for comments, so we can define paths like foo/bar/%comment in hook_menu() implementations.
 */
function dt_comment_load($cid) {
  static $comments = array();
  if (!isset($comments[$cid])) {
    $comments[$cid] = _dt_comment_load($cid);
  }
  return $comments[$cid];
}
