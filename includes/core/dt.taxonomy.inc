<?php

/**
 * @file Expanded API for taxonomy.module
 */

/**
 * Load a vocabulary by machine name
 */
function dt_taxonomy_vocabulary_load_by_machine($machine, $reset = FALSE) {
  static $vocabularies = array();
  if (!empty($reset) || !isset($vocabularies[$machine])) {
    $vid = dt_taxonomy_vocabulary_vid_by_machine($machine);
    $vocabularies[$machine] = !empty($vid) ? taxonomy_vocabulary_load($vid) : array();
  }
  return $vocabularies[$machine];
}

/**
 * Get a vocabulary vid by machine name
 */
function dt_taxonomy_vocabulary_vid_by_machine($machine, $reset = FALSE) {
  static $vocabularies = array();
  if (!empty($reset) || !isset($vocabularies[$machine])) {
    $vocabularies[$machine] = db_result(db_query_range("SELECT vid FROM {vocabulary} WHERE module IN ('%s', 'features_%s')", $machine, $machine, 0, 1));
  }
  return $vocabularies[$machine];
}
