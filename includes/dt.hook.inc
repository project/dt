<?php

/**
 * @file Expanded API for Drupal's hook system
 */

/**
 * Return the cached data of a hook
 *
 * @param $hook
 *   A hook to call.
 * @param $reset
 *   Reset cache for hook
 */
function dt_cached_hook($hook, $reset = FALSE) {
  dt_hook_include($hook);
  static $data = array();
  $cache_name = 'cached_hook_'. $hook;
  if ($reset) {
    unset($data[$hook]);
    cache_clear_all($cache_name, 'cache');
  }
  if (empty($data[$hook])) {
    $cache = cache_get($cache_name);
    if (!empty($cache) && !empty($cache->data) && !empty($cache->data)) {
      $data[$hook] = $cache->data;
    }
    else {
      $data[$hook] = module_invoke_all($hook);
      drupal_alter($hook, $data[$hook]);
      cache_set($cache_name, $data[$hook]);
    }
  }
  return $data[$hook];
}

/*
 * Return the cached data of a hook
 *
 * @param $hook
 *   A hook to call.
 * @param $modify
 *   How to handle this hook
 * @param $reset
 *   Reset cache for hook
 */
function dt_cached_hook_modify($hook, $modify = NULL, $reset = FALSE) {
  dt_hook_include($hook);
  static $data = array();
  $cache_name = 'cached_hook_'. $hook;
  if ($reset) {
    unset($data[$hook]);
    cache_clear_all($cache_name, 'cache');
  }
  if (!isset($data[$hook])) {
    $cache = cache_get($cache_name);
    if (!empty($cache) && !empty($cache->data) && !empty($cache->data)) {
      $data[$hook] = $cache->data;
    }
    else {
      $data[$hook] = array();
      if (!isset($modify)) {
        $modify = array('add_module_key' => 'module');
      }
      foreach (module_implements($hook) as $module) {
        $function = $module . '_' . $hook;
        $result = $function();
        if (isset($result) && is_array($result)) {
          // Modify each array of data returned.
          foreach ($result as $key => $info) {
            foreach ($modify as $action => $extra) {
              switch ($action) {
                case 'add_module_key':
                  $result[$key][$extra] = $module;
                  break;
                case 'call_function':
                  if (function_exists($extra)) {
                    $extra($result[$key]);
                  }
                  break;
              }
            }
          }
          $data[$hook] = array_merge_recursive($data[$hook], $result);
        }
      }
      drupal_alter($hook, $data[$hook]);
      cache_set($cache_name, $data[$hook]);
    }
  }
  return $data[$hook];
}


/**
 * Collect dt api info
 */
function dt_hook_api($hook = NULL, $reset = FALSE) {
  static $info, $by_hook = array();
  if (!isset($info)) {
    foreach (module_implements('dt_hook_api') as $module) {
      $info[$module] = module_invoke($module, 'dt_hook_api');
      foreach ($info[$module]['hooks'] as $hook) {
        if (!isset($by_hook[$hook])) {
          $by_hook[$hook] = array();
        }
        $by_hook[$hook][] = $module;
      }
    }
  }
  return ($hook ? (isset($by_hook[$hook]) ? $by_hook[$hook] : array()) : $info);
}

/**
 *
 */
function dt_hook_include($hook) {
  static $included = array();
  if (empty($included[$hook])) {
    $included[$hook] = TRUE;
    foreach (dt_hook_api($hook) as $module) {
      module_load_include('inc', $module, $module . ".dt." . $hook );
    }
  }
}
