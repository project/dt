<?php

/**
 * @file Expanded API for Drupal's theme system
 */

/**
 * Enable a theme.
 *
 * Taken from install_profile_api/core/system.inc
 *
 * Modified this to clone all existing blocks to the newly enabled theme
 *
 * @param $theme
 *   Unique string that is the name of theme.
 */
function dt_theme_enable_theme($theme) {
  drupal_clear_css_cache();

  // enable the theme
  db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", $theme);

  // The following function, although recommended, will not copy over block
  // configuration information (like 'title').  We need this, so we're doing
  // our own here.
  // system_initialize_theme_blocks($theme);

  // Clone all existing blocks into our enabled theme

  // Get every single block and put it into a structured array
  $query = db_query("SELECT * FROM {blocks}");
  $all_blocks = array();
  while ($block = db_fetch_object($query)) {
    $all_blocks[$block->module .'_'. $block->delta][$block->theme] = $block;
  }

  // Get a unique value of the block since there are duplicates for every theme
  $default_theme = variable_get('theme_default', '');
  $blocks = array();
  foreach ($all_blocks as $key => $theme_blocks) {
    if (array_key_exists($theme, $theme_blocks)) {
      // Don't do anything if the block already exists for the theme we're enabling
    }
    elseif (array_key_exists($default_theme, $theme_blocks)) {
      // Use the settings of the current default theme
      $blocks[$key] = $theme_blocks[$default_theme];
    }
    else {
      // Else just kinda pick one at random
      foreach ($theme_blocks as $block) {
        $blocks[$key] = $block;
      }
    }
  }

  // Now that we have all the blocks, lets save it to the db for our new theme
  $regions = system_region_list($theme);
  foreach ($blocks as $key => $block) {
    $block->theme = $theme;
    if (!array_key_exists($block->region, $regions)) {
      // Set to the theme's default region if region doesn't exist
      $block->region = system_default_region($theme);
    }
    db_query("INSERT INTO {blocks} (module, delta, theme, status, weight, region, visibility, pages, custom, throttle, cache, title) VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, '%s', %d, %d, %d, '%s')",
      $block->module, $block->delta, $block->theme, $block->status, $block->weight, $block->region, $block->visibility, $block->pages, $block->custom, $block->throttle, $block->cache, $block->title);
  }

  // Clear and reset all the caches, etc
  $all_themes = list_themes(TRUE);
  menu_rebuild();
  drupal_rebuild_theme_registry();

  return $all_themes[$theme];
}

/**
 * Disable a theme.
 *
 * @param $theme
 *   Unique string that is the name of theme.
 */
function dt_theme_disable_theme($theme) {
  drupal_clear_css_cache();

  // Disable theme
  db_query("UPDATE {system} SET status = 0 WHERE type = 'theme' and name = '%s'", $theme);

  // Remove all block configuration
  db_query("DELETE FROM {blocks} WHERE theme = '%s'", $theme);

  // If the theme was default, erase default
  $default_theme = variable_get('theme_default', '');
  if ($default_theme == $theme) {
    variable_del('theme_default');
  }

  // Clear and reset all the caches, etc
  $all_themes = list_themes(TRUE);
  menu_rebuild();
  drupal_rebuild_theme_registry();

  return $all_themes[$theme];
}

/**
 * Set default theme.
 *
 * Taken from install_profile_api/core/system.inc
 * Can't get install_profile_api module to work outside of an install profile.
 *
 * @param $theme
 *   Unique string that is the name of theme.
 */
function dt_theme_default_theme($theme) {
  global $theme_key;
  dt_theme_enable_theme($theme);
  variable_set('theme_default', $theme);
  // update the global variable too,
  // mainly so that block functions work correctly
  $theme_key = $theme;
}

/**
 * Gathers all enabled themes
 *
 * @return
 *   An array of enabled themes, or just the default theme
 */
function dt_theme_enabled_themes() {
  // Get all themes
  $themes = &ctools_static('dt_theme_enabled_themes', array());
  if (!$themes) {
    if ($cache = cache_get('dt_theme_enabled_themes')) {
      $themes = $cache->data;
    }
    else {
      $themes = array();
      // Put all enabled themes into an array
      foreach (list_themes() as $theme) {
        if ($theme->status == 1) {
          $themes[$theme->name] = $theme->name;
        }
      }
      cache_set('dt_theme_enabled_themes', $themes);
    }
  }
  // Add the default theme, since sometimes it isn't enabled
  $default_theme = variable_get('theme_default', 'garland');
  $themes[$default_theme] = $default_theme;
  return $themes;
}
