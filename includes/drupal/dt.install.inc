<?php

/**
 * @file Expanded API for Drupal's install process
 */

/**
 * Take a module list and add in all dependant modules
 *
 * Take an array of modules, add in their dependencies and order them
 * appropriately.
 *
 * @param Array $module_list
 *   A list of modules to install
 * @return Array
 *   A list of all modules to install including all their dependencies, ordered
 *   by dependant modules first.
 */
function dt_install_get_dependencies($module_list) {
  static $module_cache;
  $all_modules = array_unique(array_merge($module_list, module_list()));
  if (!empty($module_cache[serialize($module_list)])) {
    return $module_cache[serialize($module_list)];
  }
  $module_cache[serialize($module_list)] = array();

  // Build up dependency list.  We don't have access to the database at this
  // stage, so avoiding functions that are using queries.
  $modules = array();
  $all_modules = drupal_system_listing('\.module$', 'modules', 'name', 0);
  $modules_info = $all_modules;
  foreach ($all_modules as $module => $info) {
    $info_file = drupal_parse_info_file(dirname($all_modules[$module]->filename) .'/'. $module .'.info');
    $modules_info[$module]->info = $info_file;
    // For the purposes of generating a list of all modules to install, add in
    // all modules flagged to be automatically installed.
    if (array_key_exists('recommends', $info_file)) {
      foreach ($info_file['recommends'] as $recommends) {
        // Ensure the module is installed on the filesystem first
        if (array_key_exists($recommends, $all_modules)) {
          $modules_info[$module]->info['dependencies'][] = $recommends;
        }
      }
    }
  }
  $modules_info = _dt_install_module_build_dependencies($modules_info);

  // Create a list that includes all modules
  // Module name is the key, and the value symbolizes weight
  $weight = 0;
  $conflicts = array();
  foreach ($module_list as $key => $module) {
    // We can't do much here as we'd like as we don't know what's enabled.
    if (in_array($module, $conflicts)) {
      unset($module_list[$key]);
    }
    // grab all the conflicts for this module
    if (array_key_exists('conflicts', $modules_info[$module]->info)) {
      $conflicts = array_merge($conflicts, $modules_info[$module]->info['conflicts']);
    }
    // Add all of its dependencies
    if (array_key_exists('dependencies', $modules_info[$module]->info)) {
      $dependencies = $modules_info[$module]->info['dependencies'];
      if (isset($dependencies) && is_array($dependencies)) {
        foreach ($dependencies as $dependency) {
          if (!isset($modules[$dependency])) { // Don't overwrite previous value
            $modules[$dependency] = $weight;
            $weight++; // Increase weight for each
          }
        }
      }
    }
    // Add itself to the bottom of the list
    $modules[$module] = $weight;
    $weight++;
  }

  // Order by dependency, dependencies last
  // Borrowed from drupal_install_modules().
  do {
    $moved = FALSE;
    foreach ($modules as $module => $weight) {
      //suppress errors here to avoid the "cannot modify header" error.  It's ok, we're safety checking on the next line
      $dependencies = @$modules_info[$module]->info['dependencies'];
      if (isset($dependencies) && is_array($dependencies)) {
        foreach ($dependencies as $dependency) {
          if (isset($modules[$dependency]) && $modules[$module] < $modules[$dependency] +1) {
            $modules[$module] = $modules[$dependency] +1;

            // Ensure that this new weight didn't put the module after its dependents
            $dependents = @$modules_info[$module]->info['dependents'];
            if (isset($dependents) && is_array($dependents)) {
              foreach ($dependents as $dependent) {
                if (isset($modules[$dependent]) && $modules[$module] >= $modules[$dependent]) {
                  $modules[$dependent] = $modules[$module] +1;
                }
              }
            }

          }
        }
      }
    }
  } while ($moved);
  asort($modules);
  $modules = array_keys($modules);

  $module_cache[serialize($module_list)] = $modules;
  return $modules;
}

/**
 * Overriden due to recommands added and circular dependecies
 */
function _dt_install_module_build_dependencies($files) {
  do {
    $new_dependency = FALSE;
    foreach ($files as $filename => $file) {
      // We will modify this object (module A, see doxygen for module A, B, C).
      $file = &$files[$filename];
      if (isset($file->info['dependencies']) && is_array($file->info['dependencies'])) {
        foreach ($file->info['dependencies'] as $dependency_name) {
          // This is a nonexistent module.
          if ($dependency_name == '-circular-' || !isset($files[$dependency_name])) {
            continue;
          }
          // $dependency_name is module B (again, see doxygen).
          $files[$dependency_name]->info['dependents'][$filename] = $filename;
          $dependency = $files[$dependency_name];
          if (isset($dependency->info['dependencies']) && is_array($dependency->info['dependencies'])) {
            // Let's find possible C modules.
            foreach ($dependency->info['dependencies'] as $candidate) {
              if (array_search($candidate, $file->info['dependencies']) === FALSE) {
                // Is this a circular dependency?
                if ($candidate == $filename) {
                  // As a module name can not contain dashes, this makes
                  // impossible to switch on the module.
                  $candidate = '-circular-';
                  // Do not display the message or add -circular- more than once.
                  if (array_search($candidate, $file->info['dependencies']) !== FALSE) {
                    continue;
                  }
                }
                else {
                  // We added a new dependency to module A. The next loop will
                  // be able to use this as "B module" thus finding even
                  // deeper dependencies.
                  $new_dependency = TRUE;
                }
                $file->info['dependencies'][] = $candidate;
              }
            }
          }
        }
      }
      // Don't forget to break the reference.
      unset($file);
    }
  } while ($new_dependency);
  // Remove -circular-
  foreach ($files as &$file) {
    if (!empty($file->info) && !empty($file->info['dependencies'])) {
      $pos = array_search('-circular-', $file->info['dependencies']);
      if ($pos !== FALSE) {
        unset($file->info['dependencies'][$pos]);
      }
    }
  }
  return $files;
}


/**
 * Allows other modules to hook into the install process of a given module
 */
function dt_module_install($module) {
  // Gather information about the system's current module status
  $module_info = drupal_parse_info_file(drupal_get_path('module', $module) .'/'. $module .'.info');

  // The following may not be faster than module_exists(), but it does avoid
  // random occsions of infinate loops
  $enabled_modules = module_list();

  // Automatically enable all dependencies
  // Since some modules require their dependencies during installation, we will
  // simply preemptively enable them before proceeding with the rest of the
  // modules' installation.
  if (array_key_exists('dependencies', $module_info)) {
    $modules_enable = $module_info['dependencies'];
    $modules_enable[] = $module; // Add in itself just in case

    // Remove modules that are already enabled
    foreach ($modules_enable as $key => $module_enable) {
      if (array_key_exists($module_enable, $enabled_modules)) {
        unset($modules_enable[$key]);
      }
    }

    // Now finally enable the remaining modules
    dt_module_enable($modules_enable, _dt_module_enable_batching());
  }

  // Install all 'recommends' modules
  // These modules are not strictly dependencies, but are deemed to be
  // modules that should be installed and enabled if they exist.  This allows
  // us to automatically install modules without confusing real dependencies.
  // Do not attempt to perform these auto installs during site installation;
  // the dt_install_get_dependencies() function will automatically include
  // recommended modules.
  $site_installed = variable_get('install_time', FALSE);
  if ($site_installed && array_key_exists('recommends', $module_info)) {
    // Get all modules and their dependencies
    $all_modules = drupal_system_listing('\.module$', 'modules', 'name', 0);
    $all_modules = _module_build_dependencies($all_modules);

    $modules_install = $module_info['recommends'];
    $conflicts = dt_install_get_conflicts();
    foreach ($modules_install as $key => $module_install) {
      if (in_array($module_install, $conflicts) || !array_key_exists($module_install, $all_modules) || array_key_exists($module_install, $enabled_modules)) {
        // Remove modules that either don't exist, or already are enabled
        unset($modules_install[$key]);
      }
      else {
        // Ensure that all dependencies of the recommended module exists
        if (array_key_exists('dependencies', $all_modules)) {
          foreach ($all_modules['dependencies'] as $dependency) {
            if (!array_key_exists($dependency, $all_modules)) {
              // since the dependency doesn't exist, so we can't install the
              // recommended module
              unset($modules_install[$key]);
            }
            elseif (!array_key_exists($dependency, $enabled_modules)) {
              // Add currently disabled modules to the list of modules to install
              $modules_install[] = $dependency;
            }
          }
        }
      }
    }

    // Add in all the recomends' dependencies
    $modules_install = dt_install_get_dependencies($modules_install);

    // Now finally install the remaining modules
    include_once('./includes/install.inc');
    dt_module_enable($modules_install, _dt_module_enable_batching());
  }

  // Allow other modules to hook into this install process
  // Invokes hook_dt_module_install().
  module_invoke_all('dt_module_install', $module, $module_info);
}

/**
 * Invokes dt_module_uninstall
 */
function dt_module_uninstall($module) {
  $module_info = drupal_parse_info_file(drupal_get_path('module', $module) .'/'. $module .'.info');
  module_invoke_all('dt_module_uninstall', $module, $module_info);
}

/**
 * Grab all the conflicts.
 *
 *
 * @param Array $module_list
 *   A list of modules to install/enable
 * @return Array
 *   what modules were enabled
 */
function dt_install_get_conflicts() {
  static $conflicts;
  if (!isset($conflicts)) {
    $result = db_query('SELECT name, info from {system} where status = 1');
    $conflicts = array();
    while ($row = db_fetch_object($result)) {
      $info = unserialize($row->info);
      if (isset($info['conflicts']) && is_array($info['conflicts'])) {
        $conflicts = array_merge($conflicts, $info['conflicts']);
      }
    }
  }
  return $conflicts;
}


/**
 * @defgroup dt_hooks_install Hooks implemented for node
 * @{
 */

/**
 * Implementation of hook_dt_module_install().
 */
function dt_install_dt_module_install($module, $module_info) {
  variable_set('dt_install', 1);
}

/**
 * Implementation of hook_flush_caches().
 */
function dt_install_flush_caches() {
  if (variable_get('dt_install', 0)) {
    variable_del('dt_install');
  }
}

/**
 * @} End of "defgroup dt_hooks_install".
 */
