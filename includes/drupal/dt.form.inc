<?php

/**
 * @file Expanded API for Drupal's Form API
 */

/**
 * Removes access safely from part of a form.
 *
 * @param Array $form
 *   the form array, or part of form
 */
function dt_form_remove_access(&$form) {
  if (empty($form)) return;
  if (!in_array($form['#type'], array('hidden', 'value', ''))) {
    if ($form['#type'] == 'fieldset') {
      unset($form['#type']);
    }
    else {
      $form['#type'] = 'value';
      $form['#required'] = FALSE;
    }
  }
  if (!empty($form['#default_value'])) {
    $form['#value'] = $form['#default_value'];
  }
  unset($form['#options']);
  $form['#access'] = FALSE;
  foreach (element_children($form) as $key) {
    dt_form_remove_access($form[$key]);
  }
}
