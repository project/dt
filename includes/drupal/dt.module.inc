<?php

/**
 * @file Expanded API for Drupal's module system
 */

/**
 * Take module and enable/install
 *
 * @param Array $module
 *   A module to install/enable
 */
function dt_module_enable($module, $batch_mode = TRUE) {
  static $seen = array(TRUE => array(), FALSE => array());
  $exists = 0;
  if (is_array($module)) {
    foreach ($module as $mod) {
      $exists += dt_module_enable($mod, $batch_mode);
    }
    return $exists;
  }
  if (module_exists($module)) {
    return TRUE;
  }
  // Prevent enternal recursion
  if (!empty($seen[$batch_mode][$module])) {
    return FALSE;
  }
  $seen[$batch_mode][$module] = $module;
  $conflicts = dt_install_get_conflicts();
  if (in_array($module, $conflicts) || module_exists($module)) {
    if (function_exists('drush_print')) {
      if (module_exists($module)) {
        drush_print($module . ' module already exists so will not be enabled.');
      }
      else {
        drush_print($module . ' conflicts with an existing module so will not be enabled.');
      }
    }
    return module_exists($module);
  }
  // module should use dt_install to enable recommands or deeper
  // depedencies
  $files = module_rebuild_cache();
  // Enable dependencies
  if ($files[$module] && $files[$module]->info['dependencies'] && is_array($files[$module]->info['dependencies'])) {
    foreach ($files[$module]->info['dependencies'] as $dependency) {
      // Error out if cannot enable dependency
      if (!module_exists($dependency) && !dt_module_enable($dependency, $batch_mode) && empty($batch_mode)) {
        return FALSE;
      }
    }
  }
  if ($batch_mode) {
    $batch = array(
      'title' => t('Installing @name', array('@name' => $module)),
      'operations' => array(
        array('_dt_module_enable_batch', array($module)),
        array('_dt_module_enable_cleanup', array($module)),
      ),
    );
    $set_batch = & batch_get();
    // If there is a current set, batch adds right after
    // We want our batches in order though so trick it
    if (isset($set_batch) && isset($set_batch['current_set'])) {
      $set = $set_batch['current_set'];
      unset($set_batch['current_set']);
    }
    batch_set($batch);
    if (isset($set)) {
      $set_batch['current_set'] = $set;
    }
    return TRUE;
  }
  else {
    if (function_exists('drush_print')) {
      drush_print('Enabling ' . $module);
    }
    drupal_install_modules(array($module));
    if (!module_exists($module)) {
      module_enable(array($module));
    }
    return module_exists($module);
  }
}

function _dt_module_enable_batch($module, &$context) {
  _dt_module_enable_batching(TRUE);

  dt_module_enable($module, FALSE);
  $context['finished'] = TRUE;
  _dt_module_enable_batching(FALSE);
}

function _dt_module_enable_cleanup($module, &$context) {
  if (module_exists('features')) {
    features_flush_caches();
  }
  $context['finished'] = TRUE;
}

/**
 * Track whether batch mode is running for dt module enable.
 */
function _dt_module_enable_batching($batch = NULL) {
  static $is_batching = FALSE;
  if (isset($batch)) {
    $is_batching = $batch;
  }
  return $is_batching;
}

/**
 * Take module and enable/install
 *
 * @param Array $module
 *   A module to install/enable
 */
function dt_module_disable($module) {
  if (is_string($module)) {
    module_disable(array($module));
  }
  else {
    module_disable($module);
  }
}

/**
 * Returns the name of a module.
 */
function dt_module_name($module_name) {
  static $names = array();
  if (!empty($names[$module_name])) {
    return $names[$module_name];
  }
  // First check features
  $info = features_get_info('module', $module_name);
  if ($info && $info->info['name']) {
    $names[$module_name] = $info->info['name'];
    return $info->info['name'];
  }
  $info = db_result(db_query("SELECT info from {system} where name = '%s'", $module_name));
  if (!empty($info) && ($info = unserialize($info)) && !empty($info['name'])) {
    $names[$module_name] = $info['name'];
    return $info['name'];
  }
  return $module_name;
}
