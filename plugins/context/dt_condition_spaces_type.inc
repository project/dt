<?php

/**
 * Expose a space's type as a condition.
 */
class dt_condition_spaces_type extends context_condition {
  function condition_values() {
    $spaces_types = spaces_types();
    $types = array();
    $types['site'] = 'site';
    foreach ($spaces_types as $space_type => $info) {
      $types[$space_type] = $space_type;
    }
    asort($types);
    return $types;
  }

  function execute($space) {
    if (is_object($space)) {
      $space_type = $space->type;
    }
    else {
      $space_type = 'site';
    }
    $contexts = $this->get_contexts($space_type);
    $this->values[$space_type] = array();
    foreach ($contexts as $context) {
      $this->values[$space_type][] = $context->name;
      context_set('context', $context->name, $context);
    }
  }
}
