<?php

/**
 * Expose a space's preset as a condition.
 */
class dt_condition_spaces_preset extends context_condition {
  function condition_values() {
    $spaces_presets = spaces_preset_load();
    $presets = array();
    foreach ($spaces_presets as $preset => $info) {
      if (!$info->disabled) {
        $presets[$preset] = $info->title;
      }
    }
    asort($presets);
    return $presets;
  }

  function execute($space_preset) {
    $contexts = $this->get_contexts($space_preset);
    $this->values[$space_preset] = array();
    foreach ($contexts as $context) {
      $this->values[$space_preset][] = $context->name;
      context_set('context', $context->name, $context);
    }
  }
}
