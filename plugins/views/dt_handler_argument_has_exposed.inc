<?php

/**
 * Argument handler that ignores the argument.
 */
class dt_handler_argument_has_exposed extends views_handler_argument_null {
  function option_definition() {
    $options = parent::option_definition();
    $options['exposed_keys'] = array('default' => FALSE);
    $options['consider_empty'] = array('default' => 'All');
    return $options;
  }

  /**
   * Override options_form() so that only the relevant options
   * are displayed to the user.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['exposed_keys'] = array(
      '#type' => 'textfield',
      '#title' => t('Comma Separated Keys to check'),
      '#default_value' => !empty($this->options['exposed_keys']) ? $this->options['exposed_keys'] : '',
      '#description' => t('Enter the keys to check; will only validate true for all. If none, checks for any exposed input.'),
    );
    $form['consider_empty'] = array(
      '#type' => 'textfield',
      '#title' => t('Values that should be considered empty'),
      '#default_value' => !empty($this->options['consider_empty']) ? $this->options['consider_empty'] : 'All',
      '#description' => t('Enter the values to consider empty'),
    );
    $form['use_default_to_get_this_to_work'] = array(
      '#type' => 'markup',
      '#value' => t('Provide a default argument of anything to get this to work. (fixed entry)'),
    );
    unset($form['must_not_be']);
    unset($form['wildcard']);
    unset($form['wildcard_substitution']);
  }



  function validate_argument_basic($arg) {
    $keys = $this->options['exposed_keys'];
    $empty = explode(',', $this->options['consider_empty']);
    $test = !empty($this->view->exposed_data) ? $this->view->exposed_data  : $_GET;
    if ($keys) {
      foreach(explode(',', $keys) as $key) {
        if (empty($test[$key]) || in_array($test[$key], $empty)) {
          return FALSE;
        }
      }
    }
    return !empty($test);
  }

  /**
   * Override the behavior of query() to prevent the query
   * from being changed in any way.
   */
  function query() {}
}
