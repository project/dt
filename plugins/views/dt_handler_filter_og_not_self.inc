<?php

/**
 * Filters out the current group (you want to show a bunch of groups, but exclude the current one).
 * Normally you'd add a node->nid exclusion argument, but sometimes (aka node_relationships overrides)
 * that argument is ignored and you need to use this filter.
 */
class dt_handler_filter_og_not_self extends views_handler_filter {

  function query() {
    $table = $this->ensure_my_table();
    $this->query->add_where($this->options['group'], "$table.nid != ***CURRENT_GID***");
  }

}
