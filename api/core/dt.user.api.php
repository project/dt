<?php

/**
 * @file Hooks provided by expanded user roles api
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Documentation for hook_dt_user_role_create_role().
 *
 * Allows additional actions to take place after a role is created.
 *
 * @param String $op
 *   If a role was newly created, the $op will be 'create', otherwise a
 *   previously created role will return the $op 'update'.
 * @param String $role_name
 *   User friendly name of the role
 * @param Integer $rid
 *   The role ID of the role
 * @param Array $options
 *   Additional options to pass through during role creation.  This is where you
 *   can have specific reactions on certain options attached to the role.
 */
function hook_dt_user_role_create_role($op, $role_name, $rid, $options = array()) {
  // Your reaction code
}

/**
 * Documentation for hook_dt_user_role_remove_role().
 *
 * Allows additional actions to take place after a role is deleted.
 *
 * @param String $role_name
 *   User friendly name of the role
 * @param Integer $rid
 *   The role ID of the role
 * @param Array $options
 *   Additional options to pass through during role creation.  This is where you
 *   can have specific reactions on certain options attached to the role.
 */
function hook_dt_user_role_remove_role($role_name, $rid, $options = array()) {
  // Your reaction code
}

/**
 * Implementation of hook_dt_user_role_roles().
 */
function hook_dt_user_role_roles() {
  $roles = array();

  $roles['manager'] = array(
    // Options that will be passed through to dt_user_role_create_role().
    'foo' => 'bar',
  );
  $roles['contributor'] = array(
    // Options that will be passed through to dt_user_role_create_role().
    'baz' => 'bang',
  );

  return $roles;
}

/**
 * @} End of "addtogroup hooks".
 */
