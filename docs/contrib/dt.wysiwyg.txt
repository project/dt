= WYSIWYG API =

Expanded API for manipulating settings that the contrib module does not provide


== API ==

: dt_wysiwyg_configure_format($format_id, $editor, $settings)
  Configure WYSIWYG settings for an input format
